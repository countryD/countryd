#!/usr/bin/env bash
response=$(curl --write-out %{http_code} --silent --output /dev/null localhost:4000)

if [ "$?" -ne 0 ]; then
  echo "Connection to $SERVER on port $PORT failed"
  exit 1
else
  echo "Connection to $SERVER on port $PORT succeeded"
  exit 0
fi
